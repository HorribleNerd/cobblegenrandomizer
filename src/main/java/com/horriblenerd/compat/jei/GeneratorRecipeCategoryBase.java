package com.horriblenerd.compat.jei;

import com.horriblenerd.cobblegenrandomizer.util.Generator;
import mezz.jei.api.constants.VanillaTypes;
import mezz.jei.api.gui.builder.IRecipeLayoutBuilder;
import mezz.jei.api.gui.drawable.IDrawable;
import mezz.jei.api.gui.drawable.IDrawableStatic;
import mezz.jei.api.gui.ingredient.IRecipeSlotsView;
import mezz.jei.api.helpers.IGuiHelper;
import mezz.jei.api.recipe.IFocusGroup;
import mezz.jei.api.recipe.RecipeIngredientRole;
import mezz.jei.api.recipe.category.IRecipeCategory;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiGraphics;
import net.minecraft.client.resources.language.I18n;
import net.minecraft.network.chat.Component;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.material.Fluids;

import javax.annotation.Nonnull;

/**
 * Created by HorribleNerd on 12/11/2020
 */
public abstract class GeneratorRecipeCategoryBase<T extends GeneratorRecipeWrapper> implements IRecipeCategory<T> {

    private final IDrawableStatic background;
    private final String localizedName;
    private final IDrawable icon;
    private final ItemStack iconStack;
    private final int size;

    public GeneratorRecipeCategoryBase(IGuiHelper guiHelper, ItemStack iconStack, String localizedName, int size) {
        this.size = size;
        if (this.size == 0) {
            background = guiHelper.createBlankDrawable(52, 27);
        } else if (this.size == 1) {
            background = guiHelper.createBlankDrawable(52, 45);
        } else {
            background = guiHelper.createBlankDrawable(52, 63);
        }
        this.localizedName = localizedName;
        this.icon = guiHelper.createDrawableIngredient(VanillaTypes.ITEM_STACK, iconStack);
        this.iconStack = iconStack;
    }


    @Nonnull
    @Override
    public Component getTitle() {
        return Component.translatable(localizedName);
    }

    @Nonnull
    @Override
    public IDrawable getBackground() {
        return background;
    }

    @Nonnull
    @Override
    public IDrawable getIcon() {
        return icon;
    }


    @Override
    public void setRecipe(IRecipeLayoutBuilder builder, GeneratorRecipeWrapper recipeWrapper, IFocusGroup focuses) {

        final ItemStack outputItemStack = new ItemStack(recipeWrapper.weightedBlock.getBlock());
        final ItemStack catalystItemStack = new ItemStack(recipeWrapper.catalyst);

        int offset = 18;
        int startingX = 0;
        int startingY = 0;

        if (recipeWrapper.type == Generator.Type.COBBLESTONE) {
            if (this.size != 0) {
                startingY += 18;
            }
            // Left - Water
            builder.addSlot(RecipeIngredientRole.INPUT,startingX,startingY).addFluidStack(Fluids.WATER,1000);
            // Right - Lava
            builder.addSlot(RecipeIngredientRole.INPUT,startingX + 2 * offset,startingY).addFluidStack(Fluids.LAVA,1000);
            // Middle - Result
            builder.addSlot(RecipeIngredientRole.OUTPUT,startingX + offset - 1, startingY - 1).addItemStack(outputItemStack);
            // Bottom - Catalyst
            if (recipeWrapper.catalyst != Blocks.AIR) {
                builder.addSlot(RecipeIngredientRole.INPUT,startingX + offset - 1, startingY + offset - 1).addItemStack(catalystItemStack);
            }
        } else if (recipeWrapper.type == Generator.Type.STONE) {
            // Top - Lava
            builder.addSlot(RecipeIngredientRole.INPUT,startingX + offset,startingY).addFluidStack(Fluids.LAVA,1000);
            // Left - Water
            builder.addSlot(RecipeIngredientRole.INPUT,startingX,startingY+ offset).addFluidStack(Fluids.WATER,1000);
            // Middle - Result
            builder.addSlot(RecipeIngredientRole.OUTPUT, startingX + offset - 1, startingY + offset - 1).addItemStack(outputItemStack);
            // Bottom - Catalyst
            if (recipeWrapper.catalyst != Blocks.AIR) {
                builder.addSlot(RecipeIngredientRole.INPUT,startingX + offset - 1, startingY + 2 * offset - 1).addItemStack(catalystItemStack);
            }
        } else if (recipeWrapper.type == Generator.Type.BASALT) {
            // Left - Lava
            builder.addSlot(RecipeIngredientRole.INPUT,startingX,startingY).addFluidStack(Fluids.LAVA,1000);
            // Right - Blue Ice
            builder.addSlot(RecipeIngredientRole.INPUT,startingX + 2 * offset, startingY - 1).addItemStack(new ItemStack(Items.BLUE_ICE));
            // Middle - Result
            builder.addSlot(RecipeIngredientRole.OUTPUT,startingX + offset - 1, startingY - 1).addItemStack(outputItemStack);
            // Below - Soul Soil
            builder.addSlot(RecipeIngredientRole.INPUT,startingX + offset - 1, startingY + offset - 1).addItemStack(new ItemStack(Items.SOUL_SOIL));
        }


    }

    @Override
    public void draw(T recipe, IRecipeSlotsView recipeSlotsView, GuiGraphics guiGraphics, double mouseX, double mouseY) {
        Minecraft minecraft = Minecraft.getInstance();
        String weightString = (String.format("%s: %d", I18n.get("cobblegenrandomizer.jei.weight"), recipe.weightedBlock.getWeight().asInt()));
        int y = (1 + this.size) * 18;
        minecraft.font.drawInBatch(weightString, 2, y, 0xFF808080, false, guiGraphics.pose().last().pose(), guiGraphics.bufferSource(), net.minecraft.client.gui.Font.DisplayMode.NORMAL, 0, 15728880, false);


    }
}
